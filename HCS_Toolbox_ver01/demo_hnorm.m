%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  
%% Example of compuation of homogeneous norm and homogeneous projection
%%   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tol=1e-6; % compuational tolerance

P=[1 -1; -1 2]; % the shape matrix of the wighted Euclidean norm
if norm(P-P')>tol disp('Error: shape matrix P must be symmetric'); return; end;
if min(real(eig(P)))<tol disp('Error: shape matrix P must be positive definite'); return; end;
Gd=[2 0; 1 1]; % a dilation in R^2

%monotonicy check
if min(real(eig(P*Gd+Gd'*P)))<tol disp('Error: dilation must be monotone'); return; end;

x=[1;-1];          % vector x in R^2
nx=hnorm(x,Gd,P);   % homogeneous projection of the vector x in R^2

disp(['Hom. norm  = ', num2str(nx)]);

z=hproj(x,Gd,P);   % homogeneous projection of the vector x on the unit sphere x'*P*x=1

disp(['Hom. projection = [', num2str(z(1)),';',num2str(z(2)),']']);











