function [T nt]=block_obs(A,C)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  The function [T n_list]=block_obs(A,C) computes                 %
%%  an invertible matrix T that transforms an observable pair {A,C} %
%%  to the canonical block observability form:                      %
%%     T*A*inv(T)=[A11 A12  0  ...  0 ;                             %
%%                 A21  0  A23 ...  0 ;                             %
%%                 ... ... ... ... ...;                             % 
%%                 Ak1  0  ... ...  0 ]                             %
%%  and                                                             %
%%                 C*T=[C0 0],                                      %
%%  where                                                           %
%%     - Aij is a block of the size (ni x nj) with i,j=1,...,k      %
%%     - C0 is a full column rank  matrix (m x n1)                   %
%%  The array n_list=[n1,...,nk] contains sizes of the blocks.      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[T nt]=block_con(A',C');
T=-T';