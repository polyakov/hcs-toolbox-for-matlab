function [uh ui]=e_hpic(x, K0, K, Ki, Gd, P, mu, alpha, beta)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The function  [uh ui]=e_hpic(x, K0, K, Ki, Gd, P, mu) computes 
%% components of the Homogeneous Proportional-Integral Controller (HPIC)     
%%                                                                      
%%        u = uh + v,  dv/dt=ui   
%%
%%  uh=K0*x + nx^(1+mu)*K*d(-log(nx))*x - homogenenous component
%%  
%%  ui=nx^(1+2*mu)*Ki*d(-log(nx))*x/(x'*d'(-log(nx))*P*Gd*d(-log(nx))*x)
%%
%%  where x - a system state  and   nx - the homogenenous norm of x
%%
%%
%% The function [uh ui]=e_hpic(x, K0, K, Ki, Gd, P, mu, alpha, beta)                    
%% uses the saturation parameters alpha and beta to lower/upper bound
%% the canonical homogenenous nx     
%%                                                                      
%%     nx = max(alpha, min(beta,nx)) 
%%                                                                      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ui=zeros(size(K,1),1);
if K0==0 uh=ui;
    else uh=K0*x;
end;

if nargin==9 hn=hnorm(x,Gd,P,alpha,beta);
   elseif nargin==8 hn=hnorm(x,Gd,P,alpha);
   else hn=hnorm(x,Gd,P); 
end;

if hn>1e-20 %numerical tolerance
hpx=expm(-log(hn)*Gd)*x; %homogenenous projection of x  
uh=uh+hn^(1+mu)*K*hpx;
ui=hn^(1+2*mu)*Ki*hpx/(hpx'*P*Gd*hpx);
end;

