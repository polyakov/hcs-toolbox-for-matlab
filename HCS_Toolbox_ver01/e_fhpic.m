function [uh ui]=e_fhpic(x, K0, K, Ki, G0, P, mu1, mu2, alpha, beta)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The function  [uh ui]=fhpic(x, K0, K, Ki, G0, P, mu1, mu2) computes 
%% components of Fixed-time Homogeneous Proportional-Integral Controller (FHPIC)     
%%                                                                      
%%        u = uh + v,  dv/dt=ui   
%%
%%  uh -  FHPC
%%  
%%  ui - - integrant (as in HPIC) with dependent of x'*P*x (as in FHPC)
%%
%%
%% The function [uh ui]=e_fhpic(x, K0, K, Ki, Gd, P, mu1, mu2,  alpha, beta)                    
%% uses the saturation parameters alpha and beta to lower/upper bound
%% the canonical homogenenous nx     
%%                                                                      
%%     nx = max(alpha, min(beta,nx)) 
%%                                                                      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if x'*P*x<=1 
    Gd=eye(size(K,2))+mu1*G0;mu=mu1;
else Gd=eye(size(K,2))+mu2*G0;mu=mu2;
end;


if     nargin==10  [uh ui]=e_hpic(x,K0,K,Ki,Gd,P,mu,alpha,beta);
elseif nargin==9   [uh ui]=e_hpic(x,K0,K,Ki,Gd,P,mu,alpha);
else               [uh ui]=e_hpic(x,K0,K,Ki,Gd,P,mu);
end;


