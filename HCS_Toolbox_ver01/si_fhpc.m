function u=si_fhpc(h, x, A, B, K0, K, G0, P, mu1, mu2, alpha, beta)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The function u=si_fhpc(h, x, A, B, K0, K, G0, P, mu1, mu2) descretizes
%% semi-implicitly Fixed-time Homogeneous Proportional Controller (FHCP)     
%%                                                                      
%%          _ 
%%         /
%%        /  K0*x+nx^(1+mu1)*K*d1(-log(nx))*x       if nx<=1
%%   u = < 
%%        \  K0*x+nx^(1+mu2)*K*d2(-log(nx))*x       if nx>1
%%         \_  
%%
%%  where x - system state  and   nx - homogenenous norm of x
%%
%%
%% The function u=si_fhpc(h, x, A, B, K0, K, G0, P, mu1, mu2, alpha, beta)                    
%% uses the saturation parameters alpha, beta to lower/upper bound
%% the homogenenous norm nx     
%%                                                                      
%%     nx = max(alpha, min(beta,nx)) 
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if x'*P*x<=1 Gd=eye(size(K,2))+mu1*G0;mu=mu1;
else Gd=eye(size(K,2))+mu2*G0;mu=mu2;
end;

if nargin==12       u=si_hpc(h,x,A,B,K0,K,Gd,P,mu,alpha,beta);
  elseif nargin==11 u=si_hpc(h,x,A,B,K0,K,Gd,P,mu,alpha);
else                u=si_hpc(h,x,A,B,K0,K,Gd,P,mu);
end;


