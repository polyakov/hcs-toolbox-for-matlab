function u=c_hpc(h, n, x, A, B, K0, K, Gd, P, mu, rho)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The function  u=c_hpc(h,n, x, A, B, K0, K, Gd, P, mu, rho)  discretizes 
%% consistently (*) Homogeneous Proportional Controller (HPC)     
%%                                                                      
%%     u = K0*x + nx^(1+mu)*K*d(-ln(nx))*x 
%%
%%  where x - system state  and   nx - homogenenous norm of x
%%
%% (*) i.e., preserving convergence rate of the continuous-time system
%% 
%%    dx/dt = A*x + B*u
%%  
%%  in its descrete-time counterpart
%% 
%%      x(k+1)=Ah*x(k)+B_h*u(k)
%%   
%% Important: The consistency is guaranteed for SISO system with nilpotent
%% system matrix A (n x n). MIMO system admits consistent discretization
%%  if it can be transformed to a cascade of SISO systems with nilpotenet 
%% matrices. In all other cases, it just provide a discretization with 
%% a precision better than explicit Euler method
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if mu==0 disp('Error: homogeneity degree must be nonzero'); return; end;
tol=1e-20;
%if nargin==12    alpha=max(alpha,tol);
%else alpha=tol;
%end;

    
hn=hnorm(x,Gd,P);

if hn^(-mu)>-mu*rho*h*n-tol
F0=A+B*(K0+K)+rho*Gd;
s=log(1+mu*rho*h*n*hn^mu)/(rho*mu);
Q=expm(Gd*log(hn))*expm(-rho*Gd*s)*expm(F0*s)*expm(-Gd*log(hn));
else Q=zeros(size(A));
end;

[Ah,Bh]=ZOH(h,A,B);

W=[];R=Bh;
for i=1:n
W=[W R];
R=Ah*R;
end;

u=W'*inv(W*W')*(Q-Ah^n)*x;
m=size(B,2);
u=u(n*m-m+1:n*m);






