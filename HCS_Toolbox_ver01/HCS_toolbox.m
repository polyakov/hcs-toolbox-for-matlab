%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
%% HCS (Homogeneous Control Systems) Toolbox contains MATLAB functions
%% for design, analysis and simulation of homogeneous control systems
%%
%%
%% List of functions (for more info type 'help <name_of_function>'):
%%
%%
%%  (Homogeneous Objects)
%%
%%  hnorm        - computation of homogeneous norm 
%%  hproj        - computation of homogeneous projection
%%  hcurve       - generation of points of a homogeneous curve           
%%  hsphere      - generation of a random grid on a homogeneous sphere          
%% 
%%
%%  (Homogenenous Functions and Vector Fields)
%%
%%  
%%  is_hom_f     - numerical test of a homogeneity of a function           (*)
%%  is_hom_vf    - numerical test of a homogeneity of a vector field       (*)
%%  hcons        - CONSISTENT discretization of homogeneous vector field   (*)
%%  dd_hom_f     - data-driven homogeneity degree/dilation of function     (*)
%%  dd_hom_f     - data-driven homogeneity degree/dilation of vector field (*)
%%  
%%  (Homogeneous Control Design)
%%
%%  hpc_design   - Homogeneous Proportional Control (HPC) design       
%%  hpci_design  - Homogeneous Proportional-Integral Control (HPIC) design   
%%  hsmc_design  - Homogeneous Sliding Mode Controller (HSMC)  design       
%%  hsmci_design - design of HSMC with Integral action
%%  fhpc_design  - Fixed-time HPC design
%%  fhpic_design - Fixed-time HPIC design 
%%  lpc2hpc      - upgrading Linear Proportional Control (LPC) to HPC
%%  lpic2hpc     - upgrading Linear PI control (LPIC) to HPIC
%%
%%
%% (Discretization of Homogeneous Control )
%%
%%  e_hpc       - explicit discretization of HPC
%%  e_hpc       - semi-implicit discretization of HPC
%%  c_hpc       - consistent discretization of HPC   
%%  e_hpic      - explicit discretization of HPIC
%% si_hpic      - semi-implicit discretization of HPIC                     (*)
%%  e_hsmc      - explicit discretization of HSMC
%% si_hsmc      - semi-implicit explicit discretization of HSMC      
%%  c_hSMC      - consistent discretization of HSMC                        (*)
%%  e_hsmci     - explicit discretization of HSMC with Integral action
%% si_hsmci     - semi-implicit discretization of HSMC with Integral action(*)
%%  e_fhpc      - explicit discretization of Fixed-time HPC
%% si_fhpc      - semi-implicit discretization of Fixed-time HPC
%%  e_fhpic     - explicit discretization of Fixed-time HPIC                     
%% si_fhpic     - semi-implicit discretization of Fixed-time HPIC          (*)                 
%%
%%
%%  (Homogeneous Observer Design)
%%
%%  ho_design     - Homogeneous Observer (HO) design                         
%%  fho_design    - Fixed-time HO design                                
%%  lo2ho         - upgrading Linear Observer (LO) to HO                   
%%
%%
%% (Discretization of Homogeneous Observer)
%%
%%  e_ho          - explicit Euler discretization of HO                                
%%  e_fho         - explicit Euler discretization of FHO                     
%%  si_ho         - semi-implicit discretization of HO                                
%%  si_fho        - semi-implicit discretization of FHO                     
%%
%%
%%  (Block forms )
%%
%%  block_con    - transformation to block controlability form
%%  bloc_obs     - transformation to block bservability form
%%  trans_con    - transformation to partial block controlability form
%%  trans_con    - transformation to partial block observability form
%%  output_form  - transformation to reduced order output control system  
%%
%%
%%  (Examples)
%%
%%  To open example type 'edit <name_of_example>' in Command Line of MATLAB
%%
%%  demo_hnorm     - demo of computation of a homogeneous norm            
%%  demo_hsphere   - plot of homogeneous balls in 2D         
%%  demo_hcurve    - plot of homogeneous curve in 2D                      (*)
%%  demo_hpc       - demo of HPC design and simulation
%%  demo_hpic      - demo of HPIC design and simulation
%%  demo_hsmc      - demo of HSMC design and simulation
%%  demo_hsmci     - demo of HSMCI design and simulation
%%  demo_fhpc      - demo of FHPC design and simulation
%%  demo_fhpic     - demo of FHPIC design and simulation
%%  demo_lpc2hpc   - demo of upgrading LPC to HPC/FHPC
%%  demo_lpic2hpic - demo of upgrading LPIC to HPIC/FHPIC                 
%%  demo_ho        - demo of HO design and simulation                     
%%  demo_fho       - demo of FHO design and simulation                     
%%  demo_lo2ho     - demo of upgrading LO to HO/FHO                        
%%
%%
%% (*) - function is not realized in this version of HCS toolbox
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('HCS Toolbox for MATLAB (ver 0.1)');
disp('(please type ''help HCS_toolbox'' to print the list of functions)');
