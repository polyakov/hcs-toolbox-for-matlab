%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Example of Homogenenous Proportional Controll (HPC) design
%%   
%%  System:      dx/dt=A*x+B*u,
%%                
%%        where  
%%               x - system  state vector (n x 1)
%%               u - control input (m x 1)
%%               A - system matrix (n x n)
%%               B - control matrix (m x m)
%%      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%
%% Model of system
%%%%%%%%%%%%%%%%%%%

A= [0 1; -1 0]; % sysytem matrix (harmonic oscillator)

B= [0;    1];  % control matrix

n=2; m=1;

%%%%%%%%%%%%%%%%%%
%% HPC design
%%%%%%%%%%%%%%%%%%

rho=1; %convergence rate tuning parameter (larger rho, faster convergence) 

mu=-0.5; % the homogeneity degree  
         %     mu<0 - finite-time stability, 
         %     mu>0 - nearly fixed-time stability

[K0 K Gd P]=hpc_design(A,B,mu,rho); % design  of HPC

%K0 - homogenization feedback gain
%K  - control gain
%Gd - generator of dilation
%P  - shape matrix of the weighted Euclidean norm

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Numerical Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

t=0; Tmax=5; 
h=0.01; % sampling period

x=[1;0];
tl=[t];xl=[x];ul=[];
if mu<0 
    %computation of the setting time
    disp(['Settling Time=',num2str(hnorm(x,Gd,P)^(-mu)/(-mu*rho))]);
end;


noise=0.00; %magnitude of measurement noises

disp('Run numerical simulation...');

[Ah, Bh]=ZOH(h,A,B);

while t<Tmax
    xm=x+2*noise*(rand(n,1)-0.5);    
    
    %u=(K0+K)*xm;                 %linear control (for comparison)
    
    
    u=e_hpc(xm,K0,K,Gd,P,mu); %explicit discretization of HPC

    %u=si_hpc(h,xm,A,B,K0,K,Gd,P,mu); %semi-implicit discretization of HPC
    

    %u=c_hpc(h, 2, xm, A, B, K0, K, Gd, P, mu, rho); %consistent discretization of HPC

   
    x=Ah*x+Bh*u;                %%esimulation of the system 
    t=t+h;
    tl=[tl t];
    xl=[xl x];
    ul=[ul u];
end;
ul=[ul u];

disp('Done!');

%%norm of the state at the time instant Tmax
disp(['||x(Tmax)||=',num2str(norm(x))])


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plot simulation results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure;

axes1 = subplot(1,2,1);
hold(axes1,'on');
plot1 = plot(tl,xl,'LineWidth',2,'Parent',axes1);
set(plot1(1),'DisplayName','$x_1$');
set(plot1(2),'DisplayName','$x_2$');
ylabel('$x$','Interpreter','latex');
xlabel('$t$','Interpreter','latex');
title({'n=2'});
xlim(axes1,[0 Tmax]);
ylim(axes1,[-2 2]);
box(axes1,'on');
hold(axes1,'off');
set(axes1,'FontSize',30,'XGrid','on','YGrid','on');
legend1 = legend(axes1,'show');
set(legend1,'Interpreter','latex');


axes2 = subplot(1,2,2);
hold(axes2,'on');
plot2 = plot(tl,ul,'LineWidth',2);
ylabel('$u$','Interpreter','latex');
xlabel('$t$','Interpreter','latex');
title({'HPC,m=1'});
xlim(axes2,[0 Tmax]);
ylim(axes2,[-5 5]);
box(axes2,'on');
hold(axes2,'off');
set(axes2,'FontSize',30,'XGrid','on','YGrid','on');
