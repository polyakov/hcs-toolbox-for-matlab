function [z, s]=hproj(x,Gd,P)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The function [z, s]=hproj(x, Gd, P) computes a homogeneous projection                   
%% of a given vector x on the unit sphere z'*P*z=1, namely, finds z and s 
%%                                                            
%% such that  z=d(s)*x and x'*d'(s)P*d(s)x=1    (if x is non-zero)
%%                                                            
%% The input parameters are                                   
%%     x - vector (p x 1) which homogeneous projection has to be computed  
%%    Gd - anti-Hurwitx matrix (p x p)  being the generator of
%%          the dilation d(s)=expm(s*Gd) for any real s 
%%    P  - positive definite matrix (p x p) such that P*Gd+Gd'*P>0  
%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tol=1e-6;
if norm(P-P')>tol disp('Error: matrix P must be symmetric'); end;
if min(real(eig(P)))<tol disp('Error: matrix P must be positive definite'); end;
if min(real(eig(P*Gd+Gd'*P)))<tol disp('Error: dilation expm(s*Gd) must be monotone with respext to the norm sqrt(x^T*P*x)'); end;
 
nx=hnorm(x,Gd,P); s=0;z=0;
if nx==0 disp('Error: vector x must be nonzero'); 
else s=-log(nx); z=expm(s*Gd)*x; end;