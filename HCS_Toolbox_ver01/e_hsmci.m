function [uh ui]=e_hsmci(x, C, K0, K, Ki, Gd, P, alpha, beta)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The function [uh ui]=e_hsmci(x, C, K0, K, Ki, Gd, P) computes                  
%% Homogeneous  Sliding Mode Control with Integral action (HSMCI)       
%%                                                                      
%%     u = K0*x + sqrt(ncx)*K*d(-log(hcx))*C*x + v   (HSMC + integral term)
%%                                          (ncx- homogenenous norm of C*x) 
%%
%%                    Ki*d(-log(hcx))*C*x
%%     dv/dt= --------------------------------------                          
%%            x'*C'*d'(-log(hcx))*P*d(-log(hcx))*C*x                                             
%%
%%  where x - system state  and   ncx - homogenenous norm of C*x
%%
%% The function  u=e_hsmci(x, C, K0, K, Ki, Gd, P, alpha, beta)                    
%% uses the saturation parameters alpha and beta to lower/upper bound
%% the homogenenous norm nx     
%%                                                                      
%%     ncx = max(alpha, min(beta,ncx)) 
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin==9 [uh ui]=e_hpic(C*x,0,K,Ki,Gd,P,-0.5,alpha,beta);
   elseif nargin==8 [uh ui]=e_hpic(C*x,0,K,Ki,Gd,P,-0.5,alpha);
else [uh ui]=e_hpic(C*x,0,K,Ki,Gd,P,-0.5);
end;
uh=K0*x+uh;



