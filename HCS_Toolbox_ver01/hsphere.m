function M=hsphere(r,Gd,P,Nmax)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The function M=hsphere(r, Gd, P, Nmax) returns a randomly generated an 
%% array of points distributed on a homogeneous sphere of the radius r
%%                        
%%  The homogeneous ball of the radius r>0 is the set of point 
%%
%%           Sd(r)={z : nz<=r, where nz is homogeneous norm of z}
%%
%%
%% The input parameters are                                   
%%     r - radius of the homogeneous sphere     
%%    Gd - anti-Hurwitx matrix (p x p)  being the generator of
%%         a monotone dilation d(s)=expm(s*Gd) with a real s 
%%    P  - positive definite matrix (p x p) such that P*Gd+Gd'*P>0  
%%  Nmax - number of points on the shpere  
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tol=1e-6;
n=size(Gd,1);
if r<tol disp('Error: radius of homogenenous ball must be strctly positive'); end;
if norm(P-P')>n*tol disp('Error: matrix P must be symmetric'); end;
if min(real(eig(P)))<tol disp('Error: matrix P must be positive definite'); end;
if min(real(eig(P*Gd+Gd'*P)))<tol disp('Error: dilation expm(s*Gd) must be monotone with respext to the norm sqrt(x^T*P*x)'); end;

n=size(Gd,1);
s=log(r);
M=[];
for i=1:Nmax
    x=2*(rand(n,1)-0.5);
    if norm (x)<tol x=2*(rand(n,1)-0.5); end;
    M=[M expm(Gd*s)*hproj(x,Gd,P)];
end;   