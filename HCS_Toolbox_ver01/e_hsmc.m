function u=e_hsmc(x, C, K0, K, Gd, P, alpha, beta)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The function  u=e_hsmc(x, C, K0, K, Gd, P) explicitly discretizes                    
%% Homogeneous (unit) Sliding Mode Controller (HSMC)     
%%                                                                      
%%     u = K0*x + K*d(-ln(ncx))*x 
%%
%%  where x - system state  and   ncx - homogenenous norm of C*x
%%
%%
%% The function  u=e_hsmc(x, C, K0, K, Gd, P, alpha, beta)                    
%% uses the saturation parameters alpha and beta to lower/upper bound
%% the homogenenous norm nx     
%%                                                                      
%%     ncx = max(alpha, min(beta,ncx)) 
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if K0==0 u=zeros(size(K,1),1);
    else u=K0*x;
end;

if nargin==8 u=u+e_hpc(C*x,0,K,Gd,P,-1,alpha,beta);
   elseif nargin==7 u=u+e_hpc(C*x,0,K,Gd,P,-1,alpha);
else u=u+e_hpc(C*x,0,K,Gd,P,-1);
end;


