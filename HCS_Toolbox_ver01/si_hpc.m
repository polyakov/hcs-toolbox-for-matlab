function u=si_hpc(h, x, A, B, K0, K, Gd, P, mu, alpha, beta)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The function  u=e_hpc(h, x, A, B, K0, K, Gd, P, mu) descretizes 
%% semi-implicitly Homogeneous Proportional Controller (HPC)     
%%                                                                      
%%     u = K0*x + nx^(1+mu)*K*d(-ln(nx))*x 
%%
%%  where x - system state  and   nx - homogenenous norm of x
%%
%%
%% The function  u=si_hpc(h, A, B, x, K0, K, Gd, P, mu, alpha, beta)                    
%% uses the saturation parameters alpha and beta to lower/upper bound
%% the homogenenous norm nx     
%%                                                                      
%%     nx = max(alpha, min(beta,nx)) 
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
u=zeros(size(K,1),1);


if nargin==11 hn=hnorm(x,Gd,P,alpha,beta);
   elseif nargin==10 hn=hnorm(x,Gd,P,alpha);
   else hn=hnorm(x,Gd,P); 
end;
n=size(A,2);I_n=eye(n);

if hn>1e-20 %numerical tolerance
Kh=K0+hn^(1+mu)*K*expm(-log(hn)*Gd);
u=Kh*inv(I_n-h*(A+B*Kh))*x;
end;


